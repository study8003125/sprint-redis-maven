package com.example.springredismaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRedisMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRedisMavenApplication.class, args);
	}

}
