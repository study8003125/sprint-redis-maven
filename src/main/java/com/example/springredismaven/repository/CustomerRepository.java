package com.example.springredismaven.repository;

import com.example.springredismaven.modal.Customer;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableRedisRepositories
public interface CustomerRepository extends CrudRepository<Customer, String> {
    List<Customer> findByFirstName(String firstName);
}
